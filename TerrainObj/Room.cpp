#include "Room.hpp"

Room::Room(sf::Vector2f _crd, int _len): crd(_crd), len(_len), width(_len)
{
    int tileAmount = len * width;
    tile = std::vector<Block>();
    roomShape = sf::FloatRect(crd.x - BLOCKLEN*width/2, crd.y - BLOCKLEN*len/2, BLOCKLEN*width, BLOCKLEN*len);

    sf::Vector2f stPoint(crd.x - BLOCKLEN*(width-1)/2, crd.y - BLOCKLEN*(len-1)/2), point;
    int it = 0;

    for(int x = 0; x < len; x++)
        for(int y = 0; y < width; y++)
        {
            point = stPoint + sf::Vector2f(x*BLOCKLEN, y*BLOCKLEN);
            if((x == 0)|(x == len - 1)|(y == 0)|(y == width - 1))
                {
                    tile.push_back(Block(point));
                }
        }
}

void Room::render(sf::RenderWindow &window)
{
    for(Block i : tile)
        window.draw(i.rect);
}

bool Room::isNeighbors(Room in)
{
    if(in.roomShape.contains(sf::Vector2f(this->crd.x, this->crd.y - (this->getLen() + in.getLen())/2)))
        return true;
    if(in.roomShape.contains(sf::Vector2f(this->crd.x, this->crd.y + (this->getLen() + in.getLen())/2)))
        return true;
    if(in.roomShape.contains(sf::Vector2f(this->crd.x - (this->getWidth() + in.getWidth())/2, this->crd.y)))
        return true;
    if(in.roomShape.contains(sf::Vector2f(this->crd.x + (this->getWidth() + in.getWidth())/2, this->crd.y)))
        return true;
    return false;
}

sf::Vector2f Room::getShift(Room in)
{
    return in.crd - this->crd;
}