#ifndef DVARGEN_GAMES_HEALTH_H
#define DVARGEN_GAMES_HEALTH_H

#include <cstdlib>
#include <SFML/Graphics.hpp>

class Health {
public:
    explicit Health(size_t hp): value(hp) {};

    void setHealth(size_t health) {
        value = health;
    }
    void addHealth(size_t change) {
        value += change;
    }

    size_t value;
};


#endif  // DVARGEN_GAMES_HEALTH_H
