#include "Blocks.hpp"

/*Block::Block(sf::Vector2f _crd, sf::Texture _text): crd(_crd), texture(_text) 
{

}*/

Block::Block(sf::Vector2f _crd): crd(_crd) 
{
    rect.setPosition(crd - sf::Vector2f(len/2, len/2));
    rect.setSize(sf::Vector2f(len, len));
    rect.setOutlineColor(sf::Color::Black);
    rect.setFillColor(color);
    rectRegion = sf::FloatRect(crd.x - len/2, crd.y - len/2, len, len);
}

void Block::setColor(sf::Color color)
{
    rect.setFillColor(color);
    this->color = color;
}