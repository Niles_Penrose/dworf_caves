#ifndef DVARGEN_GAMES_PLAYER_H
#define DVARGEN_GAMES_PLAYER_H

#include <cstdlib>
#include <SFML/Graphics.hpp>
#include "Health.h"
#include "Entity.h"
#include "IEventable.h"

#define HEAD_TEXTURE "/"
#define BODY_TEXTURE "/"
#define LEGS_TEXTURE "/"

class Player : Identificator, IEventable {
public:
    explicit Player(size_t hp, size_t speed);
    void takeEvent();

private:
    Health playerHP;
    size_t playerSpeed;

private:
    sf::Vector2f velocity;  // radious-vector in Global Scope
    sf::Sprite head;
    sf::Sprite body;
    sf::Sprite legs;
};

// need to work on Animation

#endif  // DVARGEN_GAMES_PLAYER_H
