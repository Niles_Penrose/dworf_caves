#ifndef MAP
#define MAP

#include <iostream>
#include <vector>
#include <random>
#include <cmath>
#include <SFML/Graphics.hpp>
#include "Room.hpp"

#define DOORWIDE 11

class Map
{
private:
    long seed;
    std::vector<Room> rooms;
    int roomAmount = 100;
public:
    std::mt19937 RNG;
    Map(long);
    void render(sf::RenderWindow&);
};

sf::Vector2f getRNGVector(int);

#endif