#include <iostream>
#include <SFML/Graphics.hpp>
#include "TerrainObj/Room.hpp"
#include "TerrainObj/Map.hpp"
#include "TerrainObj/Render.hpp"

int main()
{
    sf::RenderWindow window(sf::VideoMode(1000, 1000), "test");
    sf::View view;

    view = window.getView();
    view.zoom(7);
    window.setView(view);

    auto seed = std::time(NULL);

    srand(seed);
    Map map(seed);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        map.render(window);
    }
}