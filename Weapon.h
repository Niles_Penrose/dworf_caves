//
// Created by gg on 30.11.2019.
//

#ifndef DVARGEN_GAMES_WEAPON_H
#define DVARGEN_GAMES_WEAPON_H

#include "GameObject.h"
#include "Health.h"

class Weapon: GameObject {
public:
    Weapon(bool range, size_t hpSealed);

private:
    Health dealDamage;  // weapon damage
    bool switchRange;  // Switching weapon type: Bow or Sword
    sf::Sprite weaponSprite;
};


#endif  // DVARGEN_GAMES_WEAPON_H
