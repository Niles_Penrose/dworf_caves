//
// Created by gg on 30.11.2019.
//

#ifndef DVARGEN_GAMES_MOB_H
#define DVARGEN_GAMES_MOB_H


#include "Identificator.h"
#include "IEventable.h"
#include "Health.h"

class Mob : Identificator, IEventable {
public:
    explicit Mob(size_t hp);
    void takeEvent();

private:
    Health mobHP;
    sf::Sprite mobSprite;
};

// need working on it

#endif  // DVARGEN_GAMES_MOB_H
