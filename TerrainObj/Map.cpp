#include "Map.hpp"

Map::Map(long _seed): seed(_seed)
{
    RNG.seed(seed);

    rooms = std::vector<Room>();
    rooms.push_back(Room(sf::Vector2f(500, 500), ROOML));
    sf::Vector2f shift = rooms[0].crd, util;

    for(int i = 1; i < roomAmount; i++)
    {
        bool isNotIntersect = true;
        switch (RNG()%4)
        {
        case 0:
            shift = shift + sf::Vector2f(0, -rooms[i-1].getWidth());
            break;

        case 1:
            shift = shift + sf::Vector2f(rooms[i-1].getLen(), 0);
            break;

        case 2:
            shift = shift + sf::Vector2f(0, rooms[i-1].getWidth());
            break;

        case 3:
            shift = shift + sf::Vector2f(-rooms[i-1].getLen(), 0);
            break;
        }

        for(Room R : rooms)
            if(R.roomShape.contains(shift))
            {
                isNotIntersect = false;
                i--;
                break;
            }
        
        if(isNotIntersect)
            rooms.push_back(Room(shift, ROOML));
    }

    for(auto X = rooms.begin(); X != rooms.end(); X++)
        for(auto Y = X+1; Y != rooms.end(); Y++)
            if(Y->isNeighbors(*X) && X != Y)
            {
                shift = Y->getShift(*X); 
                sf::FloatRect door;
                util = getRNGVector((ROOML - DOORWIDE - 1)/2);
                //std::cout << util.x << " " << util.y << std::endl;

                shift.x += BLOCKLEN*((shift.x > 0) ? -1:1)*((shift.x == 0) ? 0:1); shift.y += BLOCKLEN*((shift.y > 0) ? -1:1)*((shift.y == 0) ? 0:1);
                shift.x /= 2; shift.y /= 2;

                util.x = (shift.y != 0) ? util.x*BLOCKLEN:0;
                util.y = (shift.x != 0) ? util.y*BLOCKLEN:0;
                //std::cout << shift.x << " " << shift.y << std::endl;
                //std::cout << util.x << " " << util.y << std::endl;

                door = sf::FloatRect(shift + Y->crd - sf::Vector2f(DOORWIDE*BLOCKLEN/2, DOORWIDE*BLOCKLEN/2) + util, 
                sf::Vector2f(DOORWIDE*BLOCKLEN, DOORWIDE*BLOCKLEN));
                for(auto R = Y->tile.begin(); R != Y->tile.end();)
                    if(R->rectRegion.intersects(door))
                        R = Y->tile.erase(R);
                    else R++;

                door = sf::FloatRect(-shift + X->crd - sf::Vector2f(DOORWIDE*BLOCKLEN/2, DOORWIDE*BLOCKLEN/2) + util, 
                sf::Vector2f(DOORWIDE*BLOCKLEN, DOORWIDE*BLOCKLEN));
                for(auto R = X->tile.begin(); R != X->tile.end();)
                    if(R->rectRegion.intersects(door))
                        R = X->tile.erase(R);
                    else R++;
            }   
}

void Map::render(sf::RenderWindow &window)
{
    window.clear();
    for(Room i : rooms)
        i.render(window);
    window.display();
}

sf::Vector2f getRNGVector(int MAXMIN)
{
    return sf::Vector2f(rand()%(2*MAXMIN) - MAXMIN, rand()%(2*MAXMIN) - MAXMIN);
}