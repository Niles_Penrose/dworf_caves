//
// Created by gg on 30.11.2019.
//

#ifndef DVARGEN_GAMES_IEVENTABLE_H
#define DVARGEN_GAMES_IEVENTABLE_H


class IEventable {
public:
    virtual
    void takeEvent() = 0;
};


#endif  // DVARGEN_GAMES_IEVENTABLE_H
