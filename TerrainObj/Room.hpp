#ifndef ROOM
#define ROOM

#include "Blocks.hpp"
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>

#define ROOML 41

class Room
{
private:
    int len, width;
public:
    sf::Vector2f crd;
    sf::FloatRect roomShape;
    std::vector<Block> tile;

    Room(sf::Vector2f, int);
    Room();
    void render(sf::RenderWindow&);
    bool isNeighbors(Room);
    sf::Vector2f getShift(Room);
    float getLen() { return len * BLOCKLEN; };
    float getWidth() { return width * BLOCKLEN; };
};

#endif 