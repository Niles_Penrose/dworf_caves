//
// Created by gg on 30.11.2019.
//

#ifndef DVARGEN_GAMES_GAMEOBJECT_H
#define DVARGEN_GAMES_GAMEOBJECT_H


#include <SFML/Graphics/Sprite.hpp>
#include "Identificator.h"
#include "IEventable.h"

class GameObject: Identificator, IEventable {
public:
    virtual
    void setSprite() = 0;
    virtual
    void getSprite() = 0;
};

// need working on it

#endif  // DVARGEN_GAMES_GAMEOBJECT_H
