#ifndef BLOCK
#define BLOCK

#include <SFML/Graphics.hpp>
#define BLOCKLEN 7

class Block
{
private:
    sf::Texture texture;
    sf::Color color = sf::Color::Red;
    float len = BLOCKLEN;
public:
    sf::Vector2f crd;
    sf::RectangleShape rect;
    sf::FloatRect rectRegion;

    Block(sf::Vector2f);
    void setColor(sf::Color);
    //Block(sf::Vector2f, sf::Texture);
};

#endif